import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    //DONE PARTIALLY
    // resolve the promise with the winner when fight is over

    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;


    const firstPlayerBar = document.querySelector("#left-fighter-indicator");
    const secondPlayerBar = document.querySelector("#right-fighter-indicator");

    const { PlayerOneAttack, 
      PlayerOneBlock, 
      PlayerTwoAttack, 
      PlayerTwoBlock, 
      PlayerOneCriticalHitCombination, 
      PlayerTwoCriticalHitCombination } = controls;
      //NOT FINISHED = some shecks for block, etc.
      //NOT FINISHED = 2 * attack

      let playerOneBlocking, playerTwoBlocking;
      let playerOneAttacking, playerTwoAttacking;

    document.body.addEventListener('keydown', e => {
      console.log(e.code);
      if (e.code == PlayerOneBlock) {
        playerOneBlocking = true;
      } else if (e.code == PlayerTwoBlock) {
        playerTwoBlocking = true;
      } else if (e.code == PlayerOneAttack && !playerOneAttacking)  {
        playerOneAttacking = true;
        const damage = getDamage(firstFighter, secondFighter);
        secondFighterHealth -= damage;

        secondPlayerBar.style.width = secondFighterHealth / secondFighter.health * 100 + '%';
      } else if (e.code == PlayerTwoAttack && !playerTwoAttacking) {
        playerTwoAttacking = true;
        const damage = getDamage(secondFighter, firstFighter);
        firstFighterHealth -= damage;
        firstPlayerBar.style.width = firstFighterHealth / firstFighter.health * 100 + '%';
      } 

      if (firstFighterHealth <= 0) {
        resolve(secondFighter);
      }
      if (secondFighterHealth <= 0) {
        resolve(firstFighter);
      }

    })

    document.body.addEventListener('keyup', e => {
      if (e.code == PlayerOneBlock) {
        playerOneBlocking = false;
      } else if (e.code == PlayerTwoBlock) {
        playerTwoBlocking = false;
      } else if (e.code == PlayerOneAttack) {
        playerOneAttacking = false;
      } else if (e.code == PlayerTwoAttack) {
        playerTwoAttacking = false;
      } 

    })
  });
}

export function getDamage(attacker, defender) {
  //DONE

  const defenderPower = getBlockPower(defender);
  const attakerPower = getHitPower(attacker);
  if (defenderPower > attakerPower) {
    return 0;
  }
  return attakerPower - defenderPower;
}

export function getHitPower(fighter) {
  //DONE
  const min = 1;
  const criticalHitChance = Math.random() + min;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  //DONE
  const min = 1;
  const dodgeChance = Math.random() + min;
  const blockPower = fighter.defense * dodgeChance;
  return blockPower;
}
