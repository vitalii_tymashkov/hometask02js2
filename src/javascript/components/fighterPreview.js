import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  console.log('fighter', fighter);
  //TVV done 02
  // todo: show fighter info (image, name, health, etc.)
  const nameElement = createInfoElement("NAME: " + fighter.name);
  const healthElement = createInfoElement("HEALTH: " + fighter.health);
  const attackElement = createInfoElement("ATTACK: " + fighter.attack);
  const defenseElement = createInfoElement("DEFENCE: " + fighter.defense);
  const imageElement = createFighterImage(fighter);

  fighterElement.append(nameElement, healthElement, attackElement, defenseElement, imageElement);
  return fighterElement;
}

function createInfoElement(name) {
  const nameElement = createElement({ tagName: 'span', className: 'fighters___fighter' }); //TVV - find class
  nameElement.innerText = name;
  return nameElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
