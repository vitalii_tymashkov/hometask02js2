import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  //TVV+MDA semi-done
  // call showModal function 
  // showModal("Winner", fighter.name);
  showModal({title:fighter.name + ' WON', bodyElement: `congratulations to ${fighter.name}!`});
}
