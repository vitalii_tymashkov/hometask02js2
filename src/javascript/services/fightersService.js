import { callApi } from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';

  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {

    //TVV done 001
    // todo: implement this method 
    
    const endpointFighterDetails = `details/fighter/${id}.json`;

    try {
      const apiResult = await callApi(endpointFighterDetails);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
